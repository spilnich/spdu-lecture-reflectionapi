package com.example.annotation;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Objects;

public class AnnotationDemo {

    public static void main(String[] args) throws Exception {
        Class<AnnotationCat> demoClassObj = AnnotationCat.class;
        readAnnotationOn(demoClassObj);

        Method sayingMeowMethod = demoClassObj.getMethod("sayingSmth", String.class);
        readAnnotationOn(sayingMeowMethod);

        Method method = demoClassObj.getMethod("letDownWaterGlass", Boolean.class);
        readAnnotationOn(method);
    }

    public static void readAnnotationOn(AnnotatedElement element) {
        System.out.print("\nSearch for annotations on element type: " + element.getClass().getSimpleName());
        System.out.println(". Element name: " + getElementName(element));

        AnimalTranslator annotation = element.getAnnotation(AnimalTranslator.class);
        System.out.println("\tAnnotation ".concat(Objects.isNull(annotation) ? "not " : "").concat("found:"));

        if (Objects.isNull(annotation)) return;

        print("\tSpecies: ", annotation.species());
        print("\tLanguage: ", annotation.language());
    }

    private static String getElementName(AnnotatedElement element) {
        return element instanceof Method
                ? ((Method) element).getName()
                : ((Class) element).getSimpleName();
    }

    private static void print(String title, String value) {
        if (!value.isEmpty()) {
            System.out.print(title + value + "\n");
        }
    }
}
