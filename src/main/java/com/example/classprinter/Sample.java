package com.example.classprinter;

import java.io.Serializable;

public class Sample implements Serializable, Cloneable {
    private Integer intField;
    private Object[] arrField;

    private Sample() { }

    public Sample(Integer intField) {
        this.intField = intField;
    }

    @Deprecated
    public static void ownPublicMethod(String[] params) { }

    protected static void ownProtectedMethod(String[] params) { }

    static void ownPackagePrivateMethod(String[] params) { }

    private String ownPrivateMethod() {
        return "I'm invoked";
    }

    public Integer getIntField() {
        return intField;
    }
}

