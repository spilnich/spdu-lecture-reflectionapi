package com.example.logging;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Objects;

public class DemoLogging4 implements LoggedType {
    private int val1 = 1;
    private int val2 = 2;

    public static void main(String[] args) {
        LoggedType loggedProxy = (LoggedType) Proxy.newProxyInstance(
                DemoLogging4.class.getClassLoader(),
                DemoLogging4.class.getInterfaces(),
                new LoggingDemo(new DemoLogging4()));

        System.out.println(loggedProxy.calculate());
        System.out.println("====");
        System.out.println(loggedProxy.getVal1());
    }

    @Logged
    public int calculate() {
        return this.val1 + this.val2;
    }

    public int getVal1() {
        return val1;
    }

    private static class LoggingDemo implements InvocationHandler {
        private DemoLogging4 obj;

        public LoggingDemo(DemoLogging4 obj) {
            this.obj = obj;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Logged methodAnnotation = method.getAnnotation(Logged.class);
            if (Objects.isNull(methodAnnotation)) {
                return method.invoke(obj, args);
            }
            System.out.println("Start " + method.getName() + "()");
            return method.invoke(obj, args);
        }

    }
}
