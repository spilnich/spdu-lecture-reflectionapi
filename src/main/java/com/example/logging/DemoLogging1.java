package com.example.logging;

public class DemoLogging1 {
    private int val1 = 1;
    private int val2 = 2;

    public static void main(String[] args) {
        DemoLogging1 demo = new DemoLogging1();
        System.out.println(demo.calculate());
        System.out.println("====");
        System.out.println(demo.getVal1());
    }

    private int calculate() {
        System.out.println("Start calculate()");
        return this.val1 + this.val2;
    }

    public int getVal1() {
        System.out.println("Start getVal1()");
        return val1;
    }

}
