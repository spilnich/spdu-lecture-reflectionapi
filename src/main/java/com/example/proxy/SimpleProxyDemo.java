package com.example.proxy;

import com.example.annotation.AnnotationCat;

public class SimpleProxyDemo {

    public static void main(String[] args) {
        SimpleAnimalProxy catProxy = new SimpleAnimalProxy(new AnnotationCat());

        System.out.println(catProxy.sayingSmth("meow"));
        System.out.println(catProxy.letDownWaterGlass(true));
        System.out.println(catProxy.letDownWaterGlass(false));
    }
}
