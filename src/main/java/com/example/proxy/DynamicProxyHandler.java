package com.example.proxy;

import com.example.annotation.AnimalTranslator;
import com.example.annotation.AnnotationCat;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Objects;

import static com.example.util.LogUtil.log;
import static com.example.util.LogUtil.logAnnotationNotFound;

public class DynamicProxyHandler implements InvocationHandler {
    private Object obj;

    public DynamicProxyHandler(Object f1) {
        this.obj = f1;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log(getClass(), method, args);

        AnimalTranslator classAnnotation = obj.getClass().getAnnotation(AnimalTranslator.class);
        if (Objects.isNull(classAnnotation)) {
            logAnnotationNotFound("Class ", obj.getClass().getSimpleName(), AnimalTranslator.class);
            return method.invoke(obj, args);
        }

        if (obj instanceof AnnotationCat && method.getName().equals("letDownWaterGlass")) {
            System.out.println("Call real method");
            return ((AnnotationCat) obj).letDownWaterGlass((boolean)args[0]);
        }

        if (!classAnnotation.species().equals("Cat")) return method.invoke(obj, args);

        AnimalTranslator methodAnnotation = method.getAnnotation(AnimalTranslator.class);
//        AnimalTranslator methodAnnotation = obj.getClass()
//                .getMethod(method.getName(), args[0].getClass())
//                .getAnnotation(AnimalTranslator.class);
        if (Objects.isNull(methodAnnotation)) {
            logAnnotationNotFound("Method ", method.getName(), AnimalTranslator.class);
            return method.invoke(obj, args);
        }

        final String catSaid = String.valueOf(args[0]);

        if (method.getName().equals("sayingSmth")) {

            if (methodAnnotation.language().equals("english")) {
                if (catSaid.equals("meow meow")) return "wough";
                if (catSaid.equals("MEOW")) return method.invoke(obj, "Get off human !!!");
            }

            if (methodAnnotation.language().equals("ru")) {
                if (catSaid.equals("meow meow")) return "ГОЛОДЕН Я!";
                if (catSaid.equals("MEOW")) return method.invoke(obj, "Кожаный, иди горшок убери...");
            }
            return catSaid;
        }

        return method.invoke(obj, args);
    }
}
